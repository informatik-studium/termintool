package org.iu.torbenwetter.termintool;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/termin")
public class TerminService {
    private final TerminRepository terminRepository;

    public TerminService(TerminRepository terminRepository) {
        this.terminRepository = terminRepository;

        Calendar cal = Calendar.getInstance();
        cal.set(2022, Calendar.JULY, 26, 12, 0);
        Date start = cal.getTime();
        cal.set(2022, Calendar.JULY, 26, 12, 30);
        Date end = cal.getTime();
        Termin termin = new Termin();
        termin.setName("Termin 1");
        termin.setBeginn(start);
        termin.setEnde(end);
        this.terminRepository.save(termin);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Termin>> listTermine() {
        return ResponseEntity.ok((List<Termin>) this.terminRepository.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<Termin> createTermin(@RequestBody Termin termin) {
        this.terminRepository.save(termin);
        return ResponseEntity.ok(termin);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Termin> readTermin(@PathVariable Long id) {
        Optional<Termin> optionalTermin = this.terminRepository.findById(id);
        return optionalTermin.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Termin> updateTermin(@PathVariable Long id, @RequestBody Termin termin) {
        Optional<Termin> optionalTermin = this.terminRepository.findById(id);
        if (optionalTermin.isPresent()) {
            Termin updatedTermin = optionalTermin.get();
            updatedTermin.setName(termin.getName());
            updatedTermin.setBeginn(termin.getBeginn());
            updatedTermin.setEnde(termin.getEnde());
            this.terminRepository.save(updatedTermin);
            return ResponseEntity.ok(updatedTermin);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Termin> deleteTermin(@PathVariable Long id) {
        Optional<Termin> optionalTermin = this.terminRepository.findById(id);
        if (optionalTermin.isPresent()) {
            this.terminRepository.delete(optionalTermin.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/search/{name}")
    public ResponseEntity<List<Termin>> searchByName(@PathVariable String name) {
        return ResponseEntity.ok(this.terminRepository.findByNameContaining(name));
    }

}
