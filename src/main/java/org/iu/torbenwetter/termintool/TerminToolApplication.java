package org.iu.torbenwetter.termintool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TerminToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(TerminToolApplication.class, args);
    }

}
