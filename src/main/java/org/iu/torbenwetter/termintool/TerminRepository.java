package org.iu.torbenwetter.termintool;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TerminRepository extends CrudRepository<Termin, Long> {

    List<Termin> findByNameContaining(String name);
}
